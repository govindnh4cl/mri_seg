import os
import datetime

from logger import setup_logger
from data_handling import DataHandling
from method_00 import Method00

instance_id = datetime.datetime.now().strftime("%Y_%b_%d_%H-%M-%S")  # A time-stamp for log file, model disk-write etc
logger = setup_logger(instance_id, log_option=1)


def get_base_config():
    this_path = os.path.dirname(os.path.abspath(__file__))
    cfg = dict()

    cfg['phase'] = dict()

    # ------------------- Configurable Section starts ----------------
    cfg['method_id'] = '00'

    cfg['phase']['train'] = 0  # Flag to enable/disable training
    cfg['phase']['test'] = 1  # Flag to enable/disable testing

    if cfg['phase']['test']:
        cfg['test_sets'] = ['train', 'validation', 'test']
        cfg['test_sets'] = ['validation']

    # ------------------- Configurable Section ends ----------------
    cfg['input_shape'] = [212, 256, 48]  # Shape of the .nii images

    cfg['resource_dir'] = os.path.join(this_path, 'resources')
    cfg['temp_dir'] = os.path.join(this_path, 'temp')
    cfg['in_dir'] = os.path.join(this_path, 'dataset')

    cfg['out_model'] = os.path.join(cfg['resource_dir'], 'model.bin')

    return cfg


def create_predictor(cfg):
    """
    Initialize and return predictor object based on method_id
    :param cfg:
    :return: predictor class instance
    """
    if cfg['method_id'] == '00':
        class_name = Method00
    else:
        logger.critical('Unknown method id: {:s}'.format(cfg['method_id']))
        raise Exception

    cfg = class_name.update_cfg(cfg)  # Add custom parameters needed for initialization
    cfg = DataHandling.update_data_cfg(cfg)
    return class_name(cfg, DataHandling(cfg))


def main():
    cfg = get_base_config()
    pred_obj = create_predictor(cfg)

    if cfg['phase']['train']:
        pred_obj.train()

    if cfg['phase']['test']:
        pred_obj.test()


if __name__ == '__main__':
    main()
