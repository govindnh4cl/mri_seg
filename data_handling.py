import os
import numpy as np
import pandas as pd
import nibabel as nib


from logger import get_logger

logger = get_logger()


class DataHandling:
    def __init__(self, cfg):
        self.cfg = cfg

        if self.cfg['data_format_id'] == '1000':
            batch_size = self.cfg['batch_size']
            if not os.path.exists(os.path.join(self.cfg['resource_dir'], 'train.csv')):
                logger.critical('File {:s} does not exist. Create it using scripts/partition_dataset.py and '
                      'scripts/prepare_input_list.py'.format(os.path.join(self.cfg['resource_dir'], 'train.csv')))

            self.df_train = pd.read_csv(os.path.join(self.cfg['resource_dir'], 'train.csv'))
            self.df_train = self.df_train.sample(frac=1).reset_index(drop=True)  # Shuffle the sequence of samples
            self.train_steps = int(np.ceil(len(self.df_train)/batch_size))  # num batches per epochs

            if not os.path.exists(os.path.join(self.cfg['resource_dir'], 'validation.csv')):
                logger.critical('File {:s} does not exist. Create it using scripts/partition_dataset.py and '
                      'scripts/prepare_input_list.py'.format(os.path.join(self.cfg['resource_dir'], 'validation.csv')))

            self.df_validation = pd.read_csv(os.path.join(self.cfg['resource_dir'], 'validation.csv'))
            self.df_validation = self.df_validation.sample(frac=1).reset_index(drop=True)  # Shuffle the sequence of samples
            self.validation_steps = int(np.ceil(len(self.df_validation) / batch_size))  # num batches per epochs

            batch_size = self.cfg['test_batch_size']  # During test-phase batchsize is 1 to avoid rounding errors in accuracy measurement
            if not os.path.exists(os.path.join(self.cfg['resource_dir'], 'test.csv')):
                logger.critical('File {:s} does not exist. Create it using scripts/partition_dataset.py and '
                      'scripts/prepare_input_list.py'.format(os.path.join(self.cfg['resource_dir'], 'test.csv')))

            self.df_test = pd.read_csv(os.path.join(self.cfg['resource_dir'], 'test.csv'))
            self.test_steps = int(np.ceil(len(self.df_test)/batch_size))  # num batches per epochs

    @staticmethod
    def update_data_cfg(cfg):
        cfg['data'] = dict()
        cfg['data'][cfg['data_format_id']] = dict()

        if cfg['data_format_id'] == '1000':
            d_id = cfg['data_format_id']

            # --------- Configurable parameters start -------
            extra = 5
            # --------- Configurable parameters end ----------

            cfg['data'][d_id]['extra'] = extra
            sample_wd = 2 * extra + 1
            sample_ht = 2 * extra + 1
            cfg['data'][d_id]['batch_shape'] = (cfg['batch_size'], sample_wd, sample_ht, 1)

            return cfg

    def get_batch(self, phase):
        if self.cfg['data_format_id'] == '1000':
            extra = self.cfg['data'][self.cfg['data_format_id']]['extra']
            sample_wd = 2 * extra + 1  # Sample width and height should always be an odd number
            sample_ht = 2 * extra + 1

            if phase == 'train':
                df_inp = self.df_train
                batch_size = self.cfg['batch_size']
            elif phase == 'validation':
                df_inp = self.df_validation
                batch_size = self.cfg['batch_size']
            elif phase == 'test':
                df_inp = self.df_test
                batch_size = self.cfg['test_batch_size']
            else:
                logger.critical('Phase must be one of "train", "validation" or "test". Found: {:s}'.format(phase))
                raise AssertionError

            ids = df_inp['sub_id'].unique()
            id2idx = pd.Series(np.arange(len(ids)), index=ids, dtype=int)

            # Read images once in memory
            imgs = np.zeros([len(ids)] + self.cfg['input_shape'], dtype=np.float64)  # Dtype is float64
            for sub_id in ids:
                file_path = os.path.join(self.cfg['in_dir'], '{:s}_FLAIR_Znorm.nii'.format(str(sub_id)))
                imgs[id2idx[sub_id]] = nib.load(file_path, mmap=False).get_data()

            batch_shape = (batch_size, sample_ht, sample_wd, 1)
            batch = np.empty(batch_shape, dtype=np.float64)
            labels = np.empty(batch_size, dtype=bool)
            idx_dataset = 0
            idx_batch = 0

            inp_wd, inp_ht = imgs.shape[1], imgs.shape[2]
            mid = extra

            while True:
                sub_id = df_inp.at[idx_dataset, 'sub_id']
                x = df_inp.at[idx_dataset, 'x']
                y = df_inp.at[idx_dataset, 'y']
                z = df_inp.at[idx_dataset, 'z']

                if x >= extra:
                    xtra_l = extra
                else:
                    xtra_l = extra - x

                # 1 unit larger considering numpy non-inclusive slicing for end-interval
                if x + extra <= inp_wd:
                    xtra_r = extra + 1
                else:
                    xtra_r = inp_wd - x

                if y >= extra:
                    xtra_t = extra
                else:
                    xtra_t = extra - y

                # 1 unit larger considering numpy non-inclusive slicing for end-interval
                if y + extra <= inp_ht:
                    xtra_b = extra + 1
                else:
                    xtra_b = inp_ht - y

                batch[idx_batch, mid-xtra_l:mid+xtra_r, mid-xtra_t:mid+xtra_b, 0] = \
                    imgs[id2idx[sub_id], x-xtra_l:x+xtra_r, y-xtra_t:y+xtra_b, z]

                labels[idx_batch] = df_inp.at[idx_dataset, 'label']
                idx_batch += 1
                idx_dataset += 1

                if idx_batch == batch_size:
                    yield batch.copy(), labels.copy()
                    batch.fill(0.)  # Reset for next batch
                    labels.fill(0)  # Reset for next batch
                    idx_batch = 0

                if idx_dataset == len(df_inp):
                    idx_dataset = 0

        else:
            print('Bad data_format_id: {:s}'.format(self.cfg['data_format_id']))
            exit(0)
