import os
import numpy as np
import pandas as pd
import json

random_seed = 1337
np.random.seed(random_seed)

this_path = os.path.dirname(os.path.realpath(__file__))


def main():
    test_portion = 0.2
    validation_portion = 0.1
    train_portion = 1 - test_portion - validation_portion

    in_dir = os.path.join(this_path, '..', 'dataset')
    resource_dir = os.path.join(this_path, '..', 'resources')
    os.makedirs(resource_dir, exist_ok=True)

    list_in_files = sorted([x for x in os.listdir(in_dir) if x.endswith('_Znorm.nii')])
    out_json = os.path.join(resource_dir, 'dataset_partition.json')
    ids = [x.split('_')[0] for x in list_in_files]
    num_subjects = len(ids)

    ids = [int(x) for x in ids]
    np.random.shuffle(ids)
    train_ids = ids[0:int(train_portion * num_subjects)]
    validation_ids = ids[len(train_ids):len(train_ids)+int(validation_portion * num_subjects)]
    test_ids = ids[len(train_ids)+len(validation_ids):]

    with open(out_json, 'w') as fp:
        json.dump({'train': train_ids, 'validation': validation_ids, 'test': test_ids}, fp)
        print("Dumped json file: {:s}".format(out_json))

    return


if __name__ == '__main__':
    main()
