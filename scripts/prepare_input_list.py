import os
import numpy as np
import pandas as pd
import nibabel as nib
import json

random_seed = 1337
np.random.seed(random_seed)

this_path = os.path.dirname(os.path.realpath(__file__))


def prepare_set(partition_name, in_dir, resource_dir, input_shape):
    out_csv = os.path.join(resource_dir, partition_name + '.csv')

    # Read ids for this set
    partion_file = os.path.join(resource_dir, 'dataset_partition.json')
    if not os.path.exists(partion_file):
        print('Unable to find parttion file: {:s}'.format(partion_file))
        exit(0)

    with open(partion_file) as fp:
        partition = json.load(fp)
        ids = partition[partition_name]

    # Read images
    imgs = np.zeros([len(ids)] + input_shape, dtype=np.float64)  # Dtype is float64
    for i, sub_id in enumerate(ids):
        file_path = os.path.join(in_dir, '{:s}_FLAIR_Znorm.nii'.format(str(sub_id)))
        imgs[i] = nib.load(file_path, mmap=False).get_data()

    # Read masks (ground truth)
    masks = np.zeros([len(ids)] + input_shape, dtype=np.uint8)
    for i, sub_id in enumerate(ids):
        file_path = os.path.join(in_dir, '{:s}_FLAIR_std2_Znorm_th.nii'.format(str(sub_id)))
        masks[i] = nib.load(file_path, mmap=False).get_data()

    dict_df = dict()

    for i, sub_id in enumerate(ids):
        print('\rProcessing subject ID: {:5s}   '.format(str(sub_id)), end='')
        nonzero_indices = np.where(imgs[i] != 0)  # Ignore the voxels that are 0

        if partition_name in ['train', 'validation']:
            ''' During testing-phase, there is an imbalance between +ve (WMH) and -ve (normal) classes.
            Use undersampling to reduce its effect. Undersample the -ve class to make the number of 
            sample equal to +ve samples '''
            valid_mask = masks[i][nonzero_indices]  # Mask voxels corresponding to valid image-voxels

            positive_mask_indices = np.where(valid_mask == 1)
            negative_mask_indices = np.where(valid_mask == 0)

            positive_class_indices = np.array(
                [nonzero_indices[0][positive_mask_indices], nonzero_indices[1][positive_mask_indices],
                 nonzero_indices[2][positive_mask_indices]])
            negative_class_indices = np.array(
                [nonzero_indices[0][negative_mask_indices], nonzero_indices[1][negative_mask_indices],
                 nonzero_indices[2][negative_mask_indices]])

            use_idx = np.random.randint(low=0, high=negative_class_indices.shape[1], size=positive_class_indices.shape[1])
            negative_class_indices_undersampled = negative_class_indices[:, use_idx]

            samples = np.concatenate((positive_class_indices, negative_class_indices_undersampled), axis=1)

            df = pd.DataFrame({'sub_id': sub_id,
                               'x': samples[0],
                               'y': samples[1],
                               'z': samples[2],
                               'label': 0})
            df.loc[0:len(df)//2, 'label'] = 1
        elif partition_name == 'test':
            ''' During testing-phase all valid voxels should be used '''
            df = pd.DataFrame({'sub_id': sub_id,
                               'x': nonzero_indices[0],
                               'y': nonzero_indices[1],
                               'z': nonzero_indices[2]})
            df['label'] = masks[i][nonzero_indices]
        else:
            raise AssertionError

        dict_df[sub_id] = df

    df_full = pd.concat(dict_df.values())

    df_full.to_csv(out_csv, index=False)
    print('Total num samples: {:d}'.format(len(df_full)))
    print("Dumped CSV: {:s}".format(out_csv))


def main():
    undersampling = True

    in_dir = os.path.join(this_path, '..', 'dataset')
    resource_dir = os.path.join(this_path, '..', 'resources')
    input_shape = [212, 256, 48]

    if 0:  # Enable it to create train and validation samples
        prepare_set('train', in_dir, resource_dir, input_shape)
        prepare_set('validation', in_dir, resource_dir, input_shape)

    if 0:  # Enable it to create test samples
        prepare_set('test', in_dir, resource_dir, input_shape)


if __name__ == '__main__':
    main()
