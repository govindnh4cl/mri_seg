import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

this_path = os.path.dirname(os.path.abspath(__file__))
temp_dir = os.path.join(this_path, 'temp')
os.makedirs(temp_dir, exist_ok=True)


def plot_learning_curve_loss(train_loss, val_loss, plot_id=None, show=False):
    plt.figure(figsize=(16, 8))
    plt.plot(train_loss, label='Train')
    plt.plot(val_loss, label='Validation')
    plt.xlabel('Epochs')
    plt.title('Loss: Train and Validation')

    out_file = 'loss_train_validation.png'
    if plot_id is not None:
        out_file = '{:s}_{:s}'.format(str(plot_id), out_file)

    plt.savefig(os.path.join(temp_dir, out_file))

    if show:
        plt.show(block=False)


def plot_learning_curve_acc(train_acc, val_acc, plot_id=None, show=False):
    plt.figure(figsize=(16, 8))
    plt.plot(train_acc, label='Train')
    plt.plot(val_acc, label='Validation')
    plt.xlabel('Epochs')
    plt.title('Accuracy: Train and Validation')

    out_file = 'acc_train_validation.png'
    if plot_id is not None:
        out_file = '{:s}_{:s}'.format(str(plot_id), out_file)

    plt.savefig(os.path.join(temp_dir, out_file))

    if show:
        plt.show(block=False)
