import numpy as np
import pandas as pd
from sklearn import metrics


from logger import get_logger

logger = get_logger()


def overlap_metrics(y_true, y_pred):
    """

    :param y_true: 0 or 1
    :param y_pred: Probability scores
    :return: None
    """
    threshold = 0.5
    y_pred_bool = np.zeros(y_pred.shape, dtype=bool)
    y_pred_bool[y_pred < threshold] = False  # Threshold the predicted scores
    y_pred_bool[y_pred >= threshold] = True

    tn, fp, fn, tp = metrics.confusion_matrix(y_true, y_pred_bool).ravel()
    acc = metrics.accuracy_score(y_true, y_pred_bool)
    sensitivity = tp/(tp+fn)
    specificity = tn/(tn+fp)
    overlap_ratio = tp/(fp+fn+tp)
    dsc = (2*tp)/(2*tp+fp+fn)

    # ------------ Display results --------------
    logger.info('-------- Evaluation Results --------')
    logger.info('Confusion Matrix:')
    logger.info('\t{:6d}\t{:6d}'.format(tn, fn))
    logger.info('\t{:6d}\t{:6d}'.format(fp, tp))
    logger.info('Accuracy: {:.3f}'.format(acc))
    logger.info('Sensitivity: {:.3f}'.format(sensitivity))
    logger.info('Specificity: {:.3f}'.format(specificity))
    logger.info('Overlap Ratio: {:.3f}'.format(overlap_ratio))
    logger.info('Dice Similarity Coefficient: {:.3f}'.format(dsc))
    logger.info('------------------------------------')






