import keras
import numpy as np
import pandas as pd
from sklearn import metrics
import time

from method_base import MethodBase
from logger import get_logger
import display
import eval

logger = get_logger()


class Method00(MethodBase):
    def __init__(self, cfg, data):
        self.method_id = '00'  # Just a unique identifier for this method
        MethodBase.__init__(self, cfg, data)

        self.m = None  # Model object
        self.history = None  # Epoch history

    @staticmethod  # update_cfg must be static to allow being called without initialization
    def update_cfg(cfg):
        """
        Override base class implementation
        :param cfg:
        :return:
        """

        # --------- Configurable parameters start -------
        cfg['data_format_id'] = '1000'

        cfg['batch_size'] = 256  # Batch size for training and validation phase
        cfg['test_batch_size'] = 1024  # Batch size during testing phase
        cfg['num_epochs'] = 2
        cfg['optimizer'] = 'rmsprop'
        # --------- Configurable parameters end ----------

        return cfg

    def _get_model(self):

        inp = keras.layers.Input(shape=self.cfg['data'][self.cfg['data_format_id']]['batch_shape'][1:])
        x = keras.layers.Conv2D(2, 3, strides=(1, 1), padding='valid')(inp)

        x = keras.layers.Activation('relu')(x)
        x = keras.layers.Flatten()(x)
        x = keras.layers.Dense(1, activation='sigmoid')(x)

        m = keras.models.Model(inputs=inp, outputs=x)
        return m

    def train(self):
        # Create optimizer
        if self.cfg['optimizer'] == 'rmsprop':
            opt = keras.optimizers.RMSprop()
        else:
            opt = keras.optimizers.SGD()

        # Create callbacks: model saver
        cb_save = keras.callbacks.ModelCheckpoint(self.cfg['out_model'], monitor='val_loss', verbose=0,
                                                  save_best_only=True)
        self.m = self._get_model()
        self.m.compile(optimizer=opt, loss='binary_crossentropy', metrics=['accuracy'])

        hist = self.m.fit_generator(self.data.get_batch(phase='train'),
                                    epochs=self.cfg['num_epochs'],
                                    steps_per_epoch=self.data.train_steps,
                                    validation_data=self.data.get_batch(phase='validation'),
                                    validation_steps=self.data.validation_steps,
                                    shuffle=False,  # Samples are already shuffled
                                    callbacks=[cb_save])

        logger.info("Training complete. Saved model: {:s}".format(self.cfg['out_model']))
        # Store history information
        self.history = hist.history

        # plot the learning curves
        display.plot_learning_curve_loss(self.history['loss'], self.history['val_loss'], self.method_id)
        display.plot_learning_curve_acc(self.history['acc'], self.history['val_acc'], self.method_id)

    def test(self):
        for set_name in self.cfg['test_sets']:
            self._test_set(set_name)

    def _test_set(self, set_name):
        logger.info('Started testing')
        logger.info('Loading model from: {:s}'.format(self.cfg['out_model']))
        self.m = keras.models.load_model(self.cfg['out_model'])
        print(self.m.summary())

        if set_name == 'train':
            batch_size = self.cfg['batch_size']
            num_samples = len(self.data.df_train)
            num_batches = self.data.train_steps
        elif set_name == 'validation':
            batch_size = self.cfg['batch_size']
            num_samples = len(self.data.df_validation)
            num_batches = self.data.validation_steps
        elif set_name == 'test':
            batch_size = self.cfg['test_batch_size']
            num_samples = len(self.data.df_test)
            num_batches = self.data.test_steps
        else:
            assert(0)

        num_samples_rounded = num_batches * batch_size  # Rounded-up to batch size
        y_true = np.zeros(num_samples_rounded, dtype=bool)
        y_pred = np.zeros(num_samples_rounded, dtype=float)
        generator = self.data.get_batch(phase=set_name)
        idx = 0

        start = time.time()
        for i in range(num_batches):
            print('\rSet: {:13s}. Processing batch {:d} of {:d}\t'.format(set_name, i, num_batches), end='')
            x_batch, y_true_batch = next(generator)
            y_pred_batch = self.m.predict_on_batch(x_batch)

            y_true[idx:idx+batch_size] = y_true_batch
            y_pred[idx:idx+batch_size] = y_pred_batch[:, 0]
            idx += len(x_batch)
        end = time.time()
        logger.info('Set: {:13s}. Total time: {:d}s.'.format(set_name, int(end - start)))

        # To be precise, purge the samples that were added to round off
        y_true = y_true[:num_samples]
        y_pred = y_pred[:num_samples]

        eval.overlap_metrics(y_true, y_pred)


