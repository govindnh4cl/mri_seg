from abc import ABC, abstractmethod


from logger import get_logger

logger = get_logger()


class MethodBase(ABC):
    def __init__(self, cfg, data):
        self.cfg = cfg
        self.data = data
        pass

    @staticmethod
    @abstractmethod
    def update_cfg(cfg):
        raise NotImplementedError

    @abstractmethod
    def train(self):
        raise NotImplementedError

    @abstractmethod
    def test(self):
        raise NotImplementedError
