import os
import logging


name = ''


def setup_logger(instance_id, log_option=0):
    """

    :param instance_id: Unique identifier in case of file-write
    :param log_option: 0 for console, 1 for file-write too
    :return:
    """
    logger = logging.getLogger(name)
    formatter = logging.Formatter('{asctime:15s} {levelname:8s} {message}', style='{')
    logger.setLevel(logging.DEBUG)

    if log_option >= 1:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    log_dir = 'logs'
    os.makedirs(log_dir, exist_ok=True)

    if log_option >= 2:
        # Latest log file for easy access
        log_file = os.path.join(log_dir, "latest.log")
        fh = logging.FileHandler(log_file)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        logger.info('Dumping logs to file: {:s}'.format(log_file))

    if log_option >= 3:
        # Storing logs for archival purposes
        log_file = os.path.join(log_dir, instance_id + ".log")
        fh = logging.FileHandler(log_file)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        logger.info('Dumping logs to file: {:s}'.format(log_file))

    return logger


def get_logger():
    return logging.getLogger(name)
